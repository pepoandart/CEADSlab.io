---
title: Computational Engineering And Data Science
description: at Idaho State University
omit_header_text: true
---
{{< figure src="/images/CEADS_logo_DV_light.png" title="" >}}

## CEADS (Computational Engineering And Data Science) Research Laboratory
Directed by Dr. Leslie Kerby \
Associate Professor of Computer Science \
Affiliate Faculty in Nuclear Engineering \
Idaho State University

CEADS research and capabilities center around designing and building data-driven modeling and simulation software within science and engineering. Projects are varied and include scientific machine learning applied to nuclear reactor operation and monitoring, scientific machine learning applied to Li-ion battery performance and quantum chemistry, and the security of machine learning systems as applied in nuclear engineering. Other projects involving applications of data science, machine learning, and artificial intelligence, and computational science are welcome. 


