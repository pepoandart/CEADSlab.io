---
title: Members
featured_image: ''
description: Members of CEADS
omit_header_text: true
type: page
menu: main
---

## Current CEADS members
---
### Dr. Pepo Mena
{{< figure src="/images/Mena.jpg" width=150px class=fl >}}
Dr. Mena is a current post-doc with CEADS and has been a CEADS member since 2018. He completed both a M.S. in Nuclear Engineering and a PhD. in Applied Science & Engineering while working in CEADS. His research includes data science applications for operations management and nuclear science. He is currently teaching at Idaho State University in both management and computer science as well as performing research in nuclear cyber security as a postdoc.

---
### Kallie McLaren
{{< figure src="/images/McLaren.jpg" width=150px class=fl >}}
Kallie is currently an undergraduate enrolled at Idaho State University. She is a senior and is dual majoring in Applied Mathematics and Statistics as well as obtaining a minor in Computer Science. She is currently planning to pursue a career in data science and/or cybersecurity. Her internships in CEADS have been the EXAsmr project as well as Nuclear Cybersecurity. 

---
### Eric Hill
{{< figure src="/images/Hill.jpg" width=150px class=fl >}}
BSME student

---
### Brittany Grayson
{{< figure src="/images/Grayson.jpg" width=150px class=fl >}}
Brittany is a current PhD candidate in CEADS. Her proposal is titled: "A Machine Learning Approach to Fuel Load Optimization in the Advanced Test Reactor". She is currently a neutronics analyst at the Idaho National Laboratory.

https://www.linkedin.com/in/brittany-grayson-a40091122/

---
### Steven McDaniel
{{< figure src="/images/McDaniel.jpg" width=150px class=fl >}}
Steven McDaniel is a current PhD student in CEADS. He is a senior software engineer and patent agent with data science, machine learning and computer architecture experience.

https://www.linkedin.com/in/steven-mcdaniel-7321221/

---
### Chase Juneau
{{< figure src="/images/Juneau.jpg" width=150px class=fl >}}
Chase is a current PhD candidate in CEADS. He is currently an IT Tech at the Department of Revenue for the state of Montana. 

https://www.linkedin.com/in/chasem-juneau/
&nbsp;  
&nbsp;  

---
### Kyle Massey
{{< figure src="/images/Massey.jpg" width=150px class=fl >}}
Kyle is a MSNE student in CEADS. He is currently a senior nuclear engineer at the Idaho National Laboratory.

https://www.linkedin.com/in/kyle-massey-b9957396/
&nbsp;  
&nbsp;  

---
### Paul Gilbreath
{{< figure src="/images/Gilbreath.jpg" width=150px class=fl >}}
MSCS student

---


## Past CEADS members
---

### Shovan Chowdhury
{{< figure src="/images/Chowdhury.jpg" width=150px class=fl >}}
Shovan is a past graduate student in CEADS, studying machine learning applications in Li-ion battery performance. He has MS Mechanical Engineering at ISU; his thesis is titled: "Artificial Intelligence Based Li-ion Battery Diagnostic Platform." He is currently a PhD student at the University of Tennessee - Knoxville, on an Oak Ridge National Laboratory graduate fellowship. 

https://www.linkedin.com/in/shovan-chowdhury/

---
### Patience Lamb, past CEADS student
{{< figure src="/images/Lamb.jpg" width=150px class=fl >}}
Patience Lamb graduated Idaho State University in 2022 with a Bachelors of Science in Nuclear Engineering and Computer Science. As a part of the CEADS group, Patience helped with the development of Functional Expansion Tallies within Shift/SCALE for the ExaSMR project. Currently, she is attending the Georgia Institute of Technology for her PhD in Nuclear and Radiological Engineering, focusing on nuclear cybersecurity.

https://www.linkedin.com/in/patience-lamb/

---
### Emily Elzinga
Emily is a current undergraduate at ISU as a CS and BS Physics dual major. She has worked on the cyber nuclear AI project, as well as ExaSMR.

---
### Dr. Ryan Stewart
{{< figure src="/images/Stewart.jpg" width=150px class=fl >}}
Dr. Stewart is a past graduate student in CEADS, working in the ExaSMR project. He is currently a nuclear engineer at the Idaho National Laboratory.

https://www.linkedin.com/in/ryanstewartnuclear/
&nbsp;  
&nbsp;  

---
### Aaron Johnson
{{< figure src="/images/Johnson.jpg" width=150px class=fl >}}
Aaron is a past CEADS undergraduate and graduate student, working in the ExaSMR project. He completed a BSCS at ISU and is currently a tech at College of Eastern Idaho.

https://www.linkedin.com/in/aaron-p-johnson/

---
### Katherine Wilsdon
{{< figure src="/images/Wilsdon.jpg" width=150px class=fl >}}
Katherine is a past CEADS undergraduate student, working in the ExaSMR project. She completed a BSCS at ISU and is currently a data scientist and software engineer at the Idaho National Laboratory.

https://www.linkedin.com/in/katherine-wilsdon/

---
### Dr. Brycen Wendt
{{< figure src="/images/Wendt.jpg" width=150px class=fl >}}
Brycen is a past CEADS PhD student. His dissertation is titled: "Functional Expansions Methods: Optimizations, Characterizations, and Multiphysics Practices." He is currently an R&D engineer at ANSYS, Inc.

https://www.linkedin.com/in/brycen-wendt/

---
### Steven Wacker
{{< figure src="/images/Wacker.jpg" width=150px class=fl >}}
BSNE

---

*... and more...*


