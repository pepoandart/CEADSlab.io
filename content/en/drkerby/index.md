---
title: "Dr. Kerby"
featured_image: ''
description: About Dr. Kerby
omit_header_text: true
type: page
menu: main
---
{{< figure src="/images/Kerby.jpg" width=150px align=left class="fl" >}}
&nbsp;   
Dr. Leslie Kerby  
CEADS Director  
Associate Professor, Computer Science  
Affiliate Faculty, Nuclear Engineering  
Idaho State University  
[CV](https://gitlab.com/CEADS/DrKerby/CV/-/raw/master/Kerby_CV.pdf)

&nbsp;  
&nbsp;  

---
Highlights of Dr. Kerby and the CEADS research group accomplishments over the past 7 years at ISU: 
- Almost $1.5 million in external funding from a dozen+ contracts with Idaho National Laboratory, Oak Ridge National Laboratory, and Los Alamos National Laboratory
- Trained more than a dozen students, including graduating a handful of PhD and MS students
- Over 30 peer-reviewed publications
- Dr. Kerby has developed and taught Advanced AI Methods, Applied Neural Networks, Data Science and Applied Machine Learning, Scientific Computing, and Modern Nuclear Computing; taught Data Structures and Algorithms, Introduction to Python, Introduction to Informatics and Analytics, and Monte Carlo Methods and Applications
- Dr. Kerby was nominated to be American Nuclear Society Mathematics and Computation division chair-elect
- Dr. Kerby, invited member of panel, "Current Issues in Computational Methods - Roundtable for Mathematics and Computation Division," American Nuclear Society Winter Meeting, December 1, 2021, Washington, D.C., USA
- Dr. Kerby, gave one-day "Introduction to Data Science" workshop in CAES May 2019 with INL collaborator Josh Peterson; developed two-day "Introduction to Data Science and Machine Learning" workshop given in August 2019
- Dr. Kerby, invited speaker, "CEM and LAQGSM Event Generators: Modern Software Development", at the Cross Sections for Cosmic Rays @ CERN (XSCRC2019), November 2019 in CERN, Geneva, Switzerland
- Dr. Kerby, invited workshop, "Introduction to Data Science with Python", at the American Nuclear Society Student Conference, April 2019 in Richmond, VA  
- Dr. Kerby, invited member of panel, "Thermal Hydraulics Applications of Machine Learning and Data Science", at the American Nuclear Society Winter Meeting, November 2018 in Orlando, FL
- Dr. Kerby, first Place in the IEEE Big Data and IEEE Brain Hackathon, July 2018 in Tokyo, Japan

**Publications** \
[Expanded Analysis of Machine Learning Models for Nuclear Transient Identification Using TPOT](https://doi.org/10.1016/j.nucengdes.2022.111694)

[Evaluation of Tree-based Regression over Multiple Linear Regression for Non-normally Distributed Data in Battery Performance](https://arxiv.org/pdf/2111.02513.pdf)

[Volumetric Spherical Polynomials](https://doi.org/10.1063/1.5086695)
 
 _and more_


**Student Theses**  
Pedro Mena, PhD, 2022 \
[Auto Machine Learning Applications for Nuclear Reactors: Transient Identification, Model
Redundancy and Security](https://gitlab.com/CEADS/DrKerby/CV/-/raw/master/Dissertation_Draft_5_.pdf)

Brycen Wendt, PhD, 2018 \
[Functional Expansions Methods: Optimizations, Characterizations,and Multiphysics Practices](https://github.com/LGKerby/Docs/raw/master/WendtDissertationFinal.pdf)

Shovan Chowdhury, MS, 2022 \
[Artificial Intelligence Based Li-ion Battery Diagnostic Platform](https://gitlab.com/CEADS/DrKerby/CV/-/raw/master/Shovan_MS_Thesis__final_copy.pdf)

Chase Juneau, MS, 2019 \
[The Development of the Generalized Spallation Model](https://github.com/LGKerby/Docs/raw/master/gsmThesisFinal.pdf)  

Pedro Mena, MS, 2019 \
[Reactor Transient Classification Using Machine Learning](https://github.com/LGKerby/Docs/blob/master/Mena_Pedro_MS_Final.pdf)  


**Classes Taught**  
[Advanced AI Methods](https://gitlab.com/CEADS/DrKerby/CV/-/raw/master/AdvAI-Syllabus-Fall2022.pdf)  
[Applied Neural Networks](https://gitlab.com/CEADS/DrKerby/CV/-/raw/master/AppliedNN-Syllabus-Spring2022.pdf)  
[Data Science and Applied Machine Learning](https://gitlab.com/CEADS/DrKerby/CV/-/raw/master/Data_Science_App_ML.Syllabus.Fall2021.pdf)  
[Scientific Computing](https://gitlab.com/CEADS/DrKerby/CV/-/raw/master/Scientific_Computing-Syllabus-Spring2021.pdf)   
[Data Structures and Algorithms](https://gitlab.com/CEADS/DrKerby/CV/-/raw/master/CS2235_Syllabus_Fall2022.pdf)  
Introduction to Programming with Python  
Introduction to Informatics and Analytics   
Monte Carlo Methods and Applications  
Modern Nuclear Computing   
_and more_

**Other**  
_Co-Lead_, CAES Computing, Data, and Visualization \
_Lead_, ISU Data Science Alliance \
_Graduate Coordinator_, Department of Computer Science \
_Reviewer_, Nuclear Technology \
_Reviewer_, JOM: Journal of The Minerals, Metals Materials Society (TMS) \
_Reviewer_, IEEE Transactions on Nuclear Science \
_Reviewer_, ANS M\&C \
_Reviewer_, Nuclear Science and Engineering 

&nbsp;  
&nbsp;  
{{< figure src="/images/CEADS_logo_DV_light.png" title="" >}}
