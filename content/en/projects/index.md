---
title: "Projects"
featured_image: ''
description: Summary of Research Projects
omit_header_text: true
type: page
menu: main
---
## Current CEADS research 
---
### Cybersecurity of machine learning systems

The CEADS team has prototyped three attack surfaces: 1) inference attacks to “steal” or replicate the machine learning model with varying levels of information, 2) trojan attacks to infiltrate and falsify machine learning systems with “look-a-like” systems that have triggers for certain malicious behavior, and 3) adversarial reprogramming to minimally alter inputs that then trick machine learning systems into targeted misclassification. We found that autonomous systems are significantly vulnerable to these attacks. 

***Inference attacks***  
In recent years, interest and research in the use of machine learning (ML) systems in nuclear reactors has increased dramatically. Recent advances in computing power have allowed ML algorithms to become faster and more adept at diagnosing anomalies, or transients, in the reactor and recommending solutions. As in every area of technology, however, ML systems are vulnerable to adversarial attacks, that is, attempts to subvert the ML algorithm. In order to perform an adversarial attack, the ML model, which is usually kept secret, is needed. However, given a set of inputs representative of the original training  dataset and predictions from the model on these inputs, it is possible to train a model that behaves almost exactly like the original. This sort of attack is called Model Inference, or Model Stealing. In this report, the model being copied is
called the victim model.

The attack the assailant uses is dependent on the availability of the model. In a white box attack, the attacker has complete knowledge of the model parameters, inputs, and outputs. Gray box attacks assume some level of knowledge of the victim model along with the inputs and outputs, such as whether the model is a neural network or a k-Nearest Neighbors classifier. In black-box attacks, the attacker only has access to the inputs and outputs. Black box attacks also seem like they would have the most applicability since it is easier to gain access to a model’s inputs and outputs than the model itself. 

Machine learning algorithms similar to the ones in this project are being investigated for use in nuclear reactor systems, which is concerning because all of them aside from the neural networks proved very easy to replicate. Because even simple neural networks often have thousands of different parameters, they are harder for something like H2O or even AutoKeras to copy. Algorithms such as a k-Nearest Neighbors classifier or a decision tree are easy to copy with near near perfect accuracy because they are composed of just a few parameters. 

{{< figure src="/images/Inference.png" >}}
As of yet, the only known methods of defense against a model stealing attack are protecting the data and perhaps using more complicated models. A machine learning model may be considered a very complicated function. It is impossible to derive the function without access to some Xs and the corresponding Ys. In order to copy a model, an attacker must at the very least have a set of test data and a set of predictions on that data from the model. A conclusion that can therefore be drawn from this research is that although having a more complex model such as an RNN can help deter inference attacks, it is much more effective to protect the training and features and targets. The metrics and attack execution times for each of the best attacks on the victim models are shown in Table 18.

***Trojan attacks***  
The goal of a Trojan attack is to alter a model and have the changes unrecognizable to the users. The trojaned model still performs well on the original data even though some weights have been altered, yet the trojaned model is sensitive to "masks" or "triggers" and when they are present in the data the model will misclassify the state. This could lead to several dangers especially in a nuclear power plant. An attacker could make a machine learning algorithm for the reactor misclassify data that is originally a transient as a steady state operation when the trigger is found. The attacker could do the opposite also making a steady state operation classify as a transient when the pattern is found.

{{< figure src="/images/OriginalAsherah.png" width="150px" class="fl" >}}
In order to make this attack based on a nuclear power plant scenario, the Asherah Simulator Data from the Asherah Training GitLab repository was used. This data consists of transients and steady data. There are 97 features used such as ones related to the reactor itself, the steam generators, condenser, etc. These features are then used to predict steadiness within a simple neural network model. 0 for a transient and 1 for steady state. The normalized Asherah data can be seen in Figure 17 as an image. Padding cells in the bottom right corner have been added in order to produce a perfect 10 by 10 image. However, these padded zeros were not utilized in the creation of the trojan trigger or the algorithms. They are only added for visualization purposes of the data. A sample of the dataset used for retraining  can be seen in Figures 18 and 19. The masks can be seen in the top right corner of the last five images of each.
{{< figure src="/images/TransientTrojan.png" >}}
{{< figure src="/images/SteadyStateTrojan.png" >}}

The results of this attack on classifying masked data as transients, can be seen in Table 18. This table demonstrates the success of the attack after retraining on the mixed dataset. The trojaned model performs with high accuracy on the original data (NM) as well as high on the masked test set (M) when classifying masked data as transients. The model was then retrained to classify masked data as steady state operations. The results of this attack are show in Table 19, which indicates similar success. 
{{< figure src="/images/TransientResults.png" >}}
{{< figure src="/images/SteadyStateResults.png" >}}
Since the accuracy of the original data (NM) is the same before and after retraining, as well as the significant increase in the accuracy of the masked test set (M), the attack is determined a success.

***Adversarial reprogramming***  
Adversarial reprogramming alters "background" or "non-robust" features in the data, utilized by machine learning models, to trick these models into misclassification. Robust features are features that we (humans) are able to see and use to inform our decisions, whereas non-robust features are patterns that are indistinguishable to humans but are nearly always used by machine learning models to help with their classification. The reason they are problematic is because an image can be edited in a way to change the non-robust features - and thus confuse the model - without it being a noticeable change to a human, and all with minimal change to the inputs.

{{< figure src="/images/Cat.png" width="150px" class="fl" >}}
{{< figure src="/images/DogCat.png" width="150px" class="fl" >}} 
As a conceptual example of how this adversarial reprogramming attack works, consider the two images in Figure 50 and 51. They both retain the same overall object (a cat), but the second one has cues to the ML model to classify it as a dog.

{{< figure src="/images/Seven.png" width="150px" class="fl" >}}
{{< figure src="/images/Seven-8.png" width="150px" class="fl" >}}
{{< figure src="/images/Seven-9.png" width="150px" class="fl" >}}
As a more concrete example, an image classifier was built for the MNIST digits dataset. This particular classifier had a 100% probability for predicting the original "7" shown in Figure 14 (meaning all other digits were 0% probability). Figures 53 and 54 demonstrate the resulting adversarial images to perturb the classifier towards 8 (Figure 53) and 9 (Figure 54). Note that the perturbed misclassified probabilities are over 97% for both. 

This attack utilized the Fast Gradient Sign Method (FGSM) to train the adversarial images, with a unique twist: it utilized an iterative FGSM algorithm. To test if other machine learning models were vulnerable to this attack, besides neural network image classifiers, adversarial inputs were created and tested on a kNN (k-nearest neighbors) classifier used in predicting various states of a nuclear power plant (normal and transient behaviors). Figure 63 summarizes the results, showing the model’s prediction on the adversarial sample, the target the adversary was trying to perturb towards, and the actual class value. Of the 12 different samples, 7 of the 12 were fooled from the original label; however, only 2 of those 7 were tricked towards the correct target. Overall, while not perfect at tricking the model, this is effective to show how it is possible to disrupt a k-NN model with minimal change to the input.
{{< figure src="/images/kNN.png" >}}

--- 
### Utilizing machine-learning-accelerated models in quantum chemistry
Hydrogen is an important energy carrier resource in response to limiting greenhouse gas emissions. Solid oxide electrolysis cells, including proton-conducting and oxygen-conducting electrolyzers, are highly efficient carbon-neutral hydrogen technologies. The features central to their systems are a multitude of multiphase interactions at interfaces, which in turn influence driving forces for both desired and undesired reactions. The complexity of multiphase interactions provides numerous grand challenges in terms of understanding the detailed properties for performance improvement and materials development. Therefore, new approaches are essential to improving mechanistic understanding and predictive capability for optimization. The purpose of this project is to develop a robust predictive framework to understand multiphase interactions at solid/gas and solid/solid interfaces by coupling machine learning (ML) and quantum chemistry. The deep understanding will accelerate material screening for electrode and electrolyte and components optimization in solid oxide electrolysis cells. Metaheuristic algorithms employed in ML mitigate the concerns about the accuracy of ML predictions in materials science due to the predicament of scarce data sets. In addition, the predictive framework goes beyond common composition/structure-property-behavior relationship identification. Its transferability between systems and processes will facilitate the development of more efficient systems for energy storage, transport, and conversion.

---
### Machine learning models for prediction of pebble bed reactor run-in
The run-in process requires bringing a high temperature gas-cooled pebble bed reactor (PBR) from fresh lower enriched fuel mixed with graphite pebbles to an equilibrium core with higher enriched fuel which is recycled for multiple passes through the core. To achieve an equilibrium core, the run-in process can be adjusted to ensure high fuel utilization of low enriched fuel, rapid approach to full power, or some combination of these. This requires constant tuning during the run-in for reactor variables such as the reactor power, fuel pebble enrichment, fraction of graphite pebbles, fuel flow rate, helium inlet temperatures, and control rod positions, which is currently done manually to assess the viability of the run-in. The goal of this project is to create machine learning models which accurately predict the run-in process given a set of design parameters. 

{{< figure src="/images/PBR_AI.png" width="150px" class="fl" >}}
The run-in process for PBRs is a multi-scale time-dependent problem requiring a high-fidelity model to capture the occurring physics, while maintaining enough coarseness to allow for solutions in a reasonable time frame. A typical run-in problem has multiple input variables (reactor power, fraction of pebble type in core, transition to equilibrium fuel, etc.) all of which are a function of time. The input variables selected will determine the response of the core and produce a set of output parameters (pebble burnup, power density, criticality, etc.). Varying any of the input parameters during a time step can have a drastic impact on the viability of the run-in process. The temporal dependence inherent to the run-in of a PBR requires sophisticated techniques to capture the output parameters. 

Our work has shown that using a recurrent neural network (RNN) can provide a model that can accurately capture many of the features important to the first 365 days of a run-in problem. Figure 2 provides an example of how a RNN using three time-dependent input variables can capture the average power density for both startup and equilibrium fuel, even when equilibrium fuel is not initially present in the core. Preliminary work found that splitting the output variables into three major categories (i.e. k-eff, average power, and burnup) allowed for the creation of three RNNs with a mean absolute error around 0.01. This work provided a proof of concept and given the level of accuracy achieved with an unrefined RNN, there are many advantages to be gained by delving deeper into the creation of ML models for the PBR run-in scenario. 

--- 
### Advanced Test Reactor (ATR) fuel loading optimization with machine learning
Optimizing the fuel loading in a reactor is an important step in reducing economic costs while maintaining safety standards. Despite being well-studied for light water reactors (LWRs), fuel load optimization has had limited application to research reactors and had never been applied to the Advanced Test Reactor (ATR). The ATR is a complex research reactor and cannot benefit from symmetry due to significant power tilts in the reactor and the fact that some experimental insertions into the reactor could add or remove reactivity. Preliminary research used clustering and k-nearest neighbors to achieve two slightly different fuel loadings that were then run in the MC21 Monte Carlo code. The original fuel loading used 19 fresh fuel elements. The first simulated fuel loading used 16 fresh fuel elements and 524 fewer grams (g) 235U. The second simulated fuel loading used 17 fresh fuel elements and 303 fewer grams 235U. An iterative process was completed for the shim rotations for the first simulated fuel loading and the fuel loading was deemed viable.

{{< figure src="/images/ATR.png" >}}
A fuel loading was created using information generated by some simpler machine learning models. The first simulated fuel loading used three fewer fresh fuel elements and 524 fewer grams 235U. The second simulated fuel loading used two fewer fresh fuel elements and 303 fewer grams 235U. Increasing the gram loading for the uranium did not increase the results substantially enough and the shim rotations needed to be considered. If the first simulated fuel loading was able to produce effective results after adjusting the shim rotation, the second simulated fuel loading can also be considered to be viable, since it has more 235U. The initial results of the MC21 run were diminished by using the as-run shim rotations. Initially, the shim rotations were set to accommodate more uranium than the simulated fuel loading created. The shim rotations were increased iteratively, and the results were promising. In the future, it will be important to generate power and control simulation data because the as-run data is unavailable until after the cycle runs.

The end goal is to optimize the fuel loading to require fewer fresh fuel elements, thus reducing costs to the ATR. To use a more sophisticated machine learning model, such as neural networks, a significant amount of data simulation will need to be completed. A methodology must be established to determine the correct shim positions to optimize the fuel loading. Ultimately, the goal is to create a tool that runs quickly enough to be useful to analysts which can be used in conjunction with engineering judgement to create a safe, optimized fuel loading. 

--- 
## Past CEADS research
---
### Utilizing ML and AI to predict battery performance
- More information coming soon

---
### Utilizing machine learning to recognize nuclear reactor transients
- More information coming soon

---
### And more:
- Novel methods for data storage and transfer within computational and multiphysics codes
- Database methods for nuclear materials
- Modernization of and development of APIs for legacy codes
- Multiphysics coupling drivers and APIs

&nbsp;  
&nbsp;  

{{< figure src="/images/CEADS_logo_DV_light.png" title="" >}}
